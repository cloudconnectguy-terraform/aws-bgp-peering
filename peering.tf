# Terraform Script to build AWS infrastructure needed for AWS Demos
# Run the following commands before terraform apply
# export AWS_ACCESS_KEY_ID="anaccesskey"
# export AWS_SECRET_ACCESS_KEY="asecretkey"
# export AWS_DEFAULT_REGION="aregion"
# Note: if you add a space before the export command it will not be saved to history file on disk

# Build VPC-1

provider "aws" {
  region = var.region
}

resource "aws_vpc" "vpc-1" {
  cidr_block            = "10.10.0.0/24"
  instance_tenancy      = "default"
  enable_dns_hostnames  = true

  tags = {
    Name = "Demo-VPC-1"
  }
}

resource "aws_subnet" "subnet-1" {
  vpc_id     = aws_vpc.vpc-1.id
  cidr_block = "10.10.0.0/27"

  tags = {
    Name = "Demo-Subnet-1"
  }
}

resource "aws_route_table" "route-1" {
  vpc_id = aws_vpc.vpc-1.id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-1.id
  }

  tags = {
    Name = "Demo-Route-1"
  }
}

resource "aws_route_table_association" "route-assoc-1" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.route-1.id
}

resource "aws_internet_gateway" "igw-1" {
  vpc_id = aws_vpc.vpc-1.id
  tags = {
    Name = "Demo-IGW-1"
  }
}

resource "aws_vpn_gateway" "vgw-1" {
  vpc_id            = aws_vpc.vpc-1.id
  amazon_side_asn   = "65010"

  tags = {
    Name = "Demo-VGW-1"
  }
}

resource "aws_vpn_gateway_route_propagation" "route-prop-1" {
  vpn_gateway_id = aws_vpn_gateway.vgw-1.id
  route_table_id = aws_route_table.route-1.id
}

resource "aws_security_group" "sg-1" {
  name        = "Demo-Security Group-1"
  description = "Allow inbound traffic for VPC-1"
  vpc_id      = aws_vpc.vpc-1.id

  ingress {
    description = "HTTP from VPC-1"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "RDP from VPC-1"
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Any from 10/8"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Demo Allow SG-1"
  }
}

# Data Source queries latest Windows instance and passes to next block

data "aws_ssm_parameter" "latest-instance-1" {
  name = "/aws/service/ami-windows-latest/Windows_Server-2019-English-Full-Base"

 }

resource "aws_instance" "instance-1" {
    ami                      = data.aws_ssm_parameter.latest-instance-1.value
    instance_type            = "t2.micro"
    subnet_id                = aws_subnet.subnet-1.id
    key_name                 = "cvim_upgrade_key"
    private_ip               = "10.10.0.15"
    vpc_security_group_ids   = [aws_security_group.sg-1.id]

tags = {
    Name = "Demo-Instance-1"
  }
}

resource "aws_eip" "eip-1" {
  instance = aws_instance.instance-1.id
  vpc      = true
}


resource "aws_dx_private_virtual_interface" "vif-1" {
  
  connection_id = var.dx-connection_id
  name               = "Demo-VIF-1"
  vlan               = var.dx-vlan_id
  address_family     = "ipv4"
  bgp_asn            = 65000
  vpn_gateway_id     = aws_vpn_gateway.vgw-1.id
  bgp_auth_key       = var.bgp-auth-key
  customer_address   = "10.0.0.1/30"
  amazon_address     = "10.0.0.2/30"
}


