# Enter the region, connection ID and VLAN ID from the AWS portal for the respective DX connection

variable "region" {
}

variable "dx-connection_id" {
}

variable "dx-vlan_id" {
}

variable "bgp-auth-key" {
}
